<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page import="com.sun.xml.internal.txw2.Document"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@page import="java.sql.*"%>
<%@page import="static com.dbcon.MyDriver.*"%>
 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>
<link href="jquery.datepick.package-5.0.0/jquery.datepick.css"	rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#popupDatepicker').datepick();
        $('#popupDatepicker1').datepick();
	$('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
}
</script>

<script type="text/javascript">
var rowCount = 0;
function addMoreRows(frm) {
rowCount ++;

var recRow = '<p id="rowCount'+rowCount+'">\n\
<tr >\n\
<td style="font-size:14px; width:180px"><input name="medicine'+rowCount+'"  id="medicine'+rowCount+'" type="text" style="width:150px; "  /></td>\n\
<td style="font-size:14px; width:80px" ><input name="mpill'+rowCount+'" id="mpill'+rowCount+'" type="text" style="width:50px;margin-left:25px;"  onkeyup="totalPill1()" /></td>\n\
<td style="font-size:14px; width:80px" ><input name="apill'+rowCount+'" id="apill'+rowCount+'" type="text" style="width:50px;margin-left:25px;"  onkeyup="totalPill1()" /></td>\n\
<td style="font-size:14px; width:80px"><input name="npill'+rowCount+'" id="npill'+rowCount+'" type="text" style="width:50px;margin-left:25px;"  onkeyup="totalPill1()" /></td>\n\
<td style="font-size:14px; width:80px"><input name="total'+rowCount+'"  id="total'+rowCount+'" type="text" style="width:50px;margin-left:25px;" onkeyup="totalPill1()" /></td>\n\
</tr> \n\
<input  type="hidden" id="rcount" name="rcount" style="width:50px;margin-left:25px;" /></td>\n\
<a href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a> </p>';

jQuery('#addedRows').append(recRow);
var textbox = document.getElementById("rcount");
textbox.value=rowCount;
}

function removeRow(removeNum) {
jQuery('#rowCount'+removeNum).remove();
}

</script>
<script type="text/javascript">

function totalPill()
{
			var a = document.getElementById("mpill");
			var b = document.getElementById("apill");
			var c = document.getElementById("npill");
			var d = (+a.value) + (+b.value) + (+c.value);
			document.getElementById("total").value=d;
}
function totalPill1()
{
	for(i=1;i<=rowCount;i++)
		{
			var a = document.getElementById("mpill"+i);
			var b = document.getElementById("apill"+i);
			var c = document.getElementById("npill"+i);
			var d = (+a.value) + (+b.value) + (+c.value);
			document.getElementById("total"+i).value=d;
		}
}


</script>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script src="js/jquery.autocomplete.js"></script>
<script>
jQuery(function(){
$("#ptname").autocomplete("patientNameList.jsp");
});
</script>
 
<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 450px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 800px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.html"%>

	<div class="maincontainer">
		<h3>
			<b>Patient Treatment</b>
		</h3>
		<br />
		<form method="post" action="addTreatmentAction.jsp">
		<table class="maincontainertable">
			<tr style="height: 45px">
			
 
				<th style="text-align: right;">Patient Name&nbsp;&nbsp;</th>
				<td><input type="text" name="ptname" id="ptname" onkeyup="getPatientInfo()"></td>
				<th style="text-align: right;">contact No.&nbsp;&nbsp;</th>
				<td><input type="text" name="contact"></td>
				<th style="text-align: right;">Date&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 100px" id="popupDatepicker" name="tdate"></td>
				<td></td>
				<td></td>
			</tr>

			<tr colspan="6" style="height: 45px">
				<th style="text-align: right;">Address&nbsp;&nbsp;</th>
				<td><input type="text" name="address"></td>


				<th style="text-align: right;">Age&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 60px" name="age"></td>


				<th style="text-align: right;">Gender&nbsp;&nbsp;</th>
				<td> <input type="text" style="width: 60px" name="gender"></td></td>


			</tr>
			<tr style="height: 45px">
				<th style="text-align: right;">Weight&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 60px" name="weight"></td>
				<th style="text-align: right;">Pulse&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 60px" name="pluse"></td>
				<th style="text-align: right;">B.P. (max)&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 60px" name="bpmax"></td>
				<th style="text-align: right;">B.P (min)&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 60px" name="bpmin"></td>

			</tr>
		</table>

		<h3>
			<b>Medications</b>
		</h3>
		<div class="medications" id="target">
			<table>
				<tr>
					<td style="font-size: 14px; width: 180px">RX</td>
					<td style="font-size: 14px; width: 80px">Morning</td>
					<td style="font-size: 14px; width: 80px">Afternoon</td>
					<td style="font-size: 14px; width: 80px">Night</td>
					<td style="font-size: 14px; width: 80px">Total</td>
					<td><span
						style="font: normal 12px agency, arial; color: blue; text-decoration: underline; cursor: pointer;"
						onclick="addMoreRows(this.form);"> Add More </span></td>
				</tr>
				<tr id="rowId">
					<td style="width: 180px"><input name="medicine" id="medicine" type="text" value="" style="width: 150px"    /></td>
					<td style="width: 80px"><input name="mpill" id="mpill" type="text" value="" style="width: 50px"  onkeyup="totalPill()" /></td>
					<td style="width: 80px"><input name="apill"  id="apill" type="text" value="" style="width: 50px" onkeyup="totalPill()"/></td>
					<td style="width: 80px"><input name="npill" id="npill" type="text" value="" style="width: 50px" onkeyup="totalPill()"/></td>
					<td style="width: 80px"><input name="total" id="total"  type="text" value="" style="width: 50px" onkeyup="totalPill()"  /></td>
			</table>
			<br />
			<div id="addedRows"></div>
			</td>
			</tr>


		
		</div>
		<!--Medication-->
		<br /> <br />

		<table class="endtable">
			<tr>
				<th style="text-align: right;"><p style="margin-bottom: 55px">Description&nbsp;&nbsp;&nbsp;</p></th>
				<td><textarea rows="3" cols="30" name="descripation">Enter Description here...</textarea></td>
				<th>Fees</th>
				<td><input type="text" style="width: 150px" name="fees"></td>
				<th></th>

			</tr>
			<tr>
				<th style="text-align: right;">Next Appointment/Visit&nbsp;&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 150px" id="popupDatepicker1" name="nextdate"></td>
				<th></th>

				<td><input type='image' height='50' width="130" src='images/submit.png'></td>

			</tr>
		</table>
		
		</form>
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic - All Rights Reserved.</div>
</body>
</html>
s