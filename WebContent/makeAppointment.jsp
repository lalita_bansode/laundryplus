<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 200px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 750px;
	height: 200px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->
<link href="jquery.datepick.package-5.0.0/jquery.datepick.css"
	rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#popupDatepicker').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
}
</script>

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.html"%>

	<div class="maincontainer">
		<h3>
			<b>Make Appointment</b>
		</h3>
		<br />


		<table class="endtable">

			<tr>
				<th style="text-align: right;">Name of Patient&nbsp;&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 150px" name="ptname"></td>
				<th style="text-align: right;">Contact No.&nbsp;&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 150px" name="ptname"></td>
			</tr>
			<tr>
				<th style="text-align: right;">Date of
					Appointment&nbsp;&nbsp;&nbsp;<br></th>
				<td><input type="text" style="width: 150px"
					id="popupDatepicker" name="ptname"></td>
				<th style="text-align: right;">Appointment
					Details&nbsp;&nbsp;&nbsp;<br /> <br /> <br />
				</th>
				<td><textarea rows="3" cols="30" name="comment" form="usrform"></textarea>
				</td>
			<tr>
				<th style="text-align: right;">Status of Appointment&nbsp;&nbsp;&nbsp;</th>
				<td><input type="text" style="width: 150px" name="ptname"></td>
				
			</tr>

			
			<tr>
				<th style="text-align: right;">Time&nbsp;&nbsp;&nbsp;</th>
				<td><select name="min">
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="08">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>


				</select> &nbsp;<b>:</b>&nbsp; <select value="seconds">
						<option value="00">00</option>
						<option value="15">15</option>
						<option value="30">30</option>
						<option value="45">45</option>
				</select></td>
				<td></td>
				<td><input type='image' height='50' width="130"
					src='images/submit.png'></td>
			</tr>



		</table>
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
