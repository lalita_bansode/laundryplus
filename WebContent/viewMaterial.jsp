<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="static com.dbcon.MyDriver.*"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 200px;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.html"%>

	<div class="maincontainer">
		<h3>
			<b>View Item Stock</b>
		</h3>
		<br />

		<form action="viewMaterialAddAction.jsp">

			<table border="0" cellspacing="2" cellspading="1" bgcolor="gray"
				width="600">


				<tr style="background-color: gray; color: white">
					<td>Id</td>
					<td>category</td>
					<td>Item Name</td>
					<td>Quantity</td>



				</tr>


				<%
             try
   {
    
       String sql= "select * from additem";
              
    
       Class.forName(DB_DRIVER);
     		Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS); 
    Statement st=con.createStatement();
    ResultSet rs = st.executeQuery(sql);
    
    while(rs.next())
   
    {%>

				<tr style="background-color: antiquewhite">
					<td><%=rs.getInt("id")%></td>



					<td>
						<% 
                
                
                int catId = Integer.parseInt(""+rs.getBigDecimal("category"));
                String sqlCat = "select itemcategory from category where id="+catId;
                Statement st2=con.createStatement();
                ResultSet rs2 = st2.executeQuery(sqlCat);
                rs2.next();
                out.println(rs2.getString("itemcategory"));
               %>


					</td>



					<td><%=rs.getString("itemname")%></td>
					<td><%=rs.getInt("itemquantity")%></td>



				</tr>

				<%}
    
    
    st.close();
    con.close();
   }
   catch(Exception e)
    
   {
       out.println(e);
      
   }
    
  %>

			</table>

		</form>
	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
</body>
</html>
