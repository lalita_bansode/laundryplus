

<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.DateFormat" %>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


<style>
.footer {
	height: 40px;
	width: 100%;
	color: white;
	background-color: #191919;
	margin-top: 12%;
	text-align: center;
}

.styleP {
	font-size: 50px;
	color: red;
}

.maincontainer {
	height: 80%;
	width: 90%;
	margin-left: 10%;
}

.maincontainertable {
	width: 85%;
	background-color: #79b7e7;
}

.medications {
	border: 2px;
	alignment-adjust: auto;
	margin-left: 10%;
	overflow: scroll;
	width: 600px;
	height: 180px;
}

body {
	background-color: #cccccc;
}

.endtable {
	width: 700px;
	height: 250px;
	margin-left: 5%;
}
</style>

<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->
<link href="jquery.datepick.package-5.0.0/jquery.datepick.css"
	rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#popupDatepicker').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
}
</script>

<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
</head>
<body>

	<%@include file="menu.html"%>

	<div class="maincontainer">
		<h3>
			<b>Update User</b>
		</h3>
		<br />
             
             
             
             
			 <form method="post" action="updateServletUser" enctype="multipart/form-data">
			 <%
			 
			    String sid=request.getParameter("id");			 
			 	String sname=request.getParameter("name");
			 	// System.out.println("name = " + sname);
	         	String sgender=request.getParameter("gender");
	         	String sdob=request.getParameter("dob");
	         	String saddress=request.getParameter("address");
	         	String scontact=request.getParameter("contact");
	         	String semail=request.getParameter("email");
	        	String sdesg=request.getParameter("designation");
	         	//String susername=request.getParameter("uname");
	         	String spassword=request.getParameter("pass");
	         	
	         	
	         	String ds1 = "2007-06-30";
	    		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	    		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
	    		String ds2 = sdf2.format(sdf1.parse(sdob));
			 %>
			 
			 
			 
			 
			 
			 
			 <input type="hidden" value="<%=sid %>" name="id">
			<table class="endtable">

				<tr>
					<th style="text-align: right;">Name&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px" name="name" value=<%=sname %>></td>
					<th style="text-align: right;">Email or &nbsp;&nbsp;&nbsp; <br />Username&nbsp;&nbsp;&nbsp;
					</th>
					<td><input type="text" style="width: 150px" name="email"value=<%=semail %>></td>
				</tr>

				<tr>
					<th style="text-align: right;">Gender&nbsp;&nbsp;&nbsp;</th>
					<td><select name="gender" style="width: 150px">
							<option value=<%=sgender %>><%=sgender %></option>
							<option value="Female">Female</option>
							<option value="Male">Male</option>
					</select></td>
					
					<th style="text-align: right;">Designation&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px" name="designation"value=<%=sdesg %>></td>
				</tr>
				
				
				<tr>

					<th style="text-align: right;">Date of Birth&nbsp;&nbsp;&nbsp;

					</th>
					<td><input type="text" style="width: 150px" id="popupDatepicker" name="dob"value=<%=ds2 %>></td>

				</tr>
				
				
				<tr>
					<th style="text-align: right;">Contact No&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px" name="contact"value=<%=scontact %>></td>
					<th style="text-align: right;">Password&nbsp;&nbsp;&nbsp;</th>
					<td><input type="text" style="width: 150px" name="pass"value=<%=spassword %>></td>
				</tr>
				
				
				<tr>
					<th style="text-align: right;">Profile Picture&nbsp;&nbsp;&nbsp;</th>
					<td><input type="file" style="width: 250px" name="pphoto" value="Browse"></td>
					<th style="text-align: right;">Confirm &nbsp;&nbsp;&nbsp; <br />Password&nbsp;&nbsp;&nbsp;
					</th>
					<td><input type="text" style="width: 150px"
						id="popupDatepicker" name="pass"></td>
				</tr>
				
				
				<tr>
					<th style="text-align: right;"></th>
					<td><img src="<%=request.getContextPath()%>/images/test.jpg" style="width: 100px; height: 100px" name="ptname"></td>
					<th style="text-align: right;">Address&nbsp;&nbsp;&nbsp;<br /><br /> <br /> <br /> <br /> <br /></th>
					
					<td><textarea rows="3" cols="30" name="address"><%=saddress.trim()%></textarea>
						<br /> <input type='image' height='50' width="130"
						src='images/submit.png'><br /></td>
				</tr>



			</table>
		</form>
		<%@include file="showUser.jsp"%>

	</div>
	<div class="footer">

		<div style="padding: 10px 5px 15px 20px;">© 2014 Scripting Logic
			- All Rights Reserved.</div>
	</div>
	>
</body>
</html>
