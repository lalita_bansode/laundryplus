<%-- 
    Document   : patient
    Created on : May 25, 2015, 12:36:15 AM
    Author     : amol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.DateFormat" %>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doctor Plus</title>

<script src="js/jquery.min.js"></script>


        <style>
            .footer {
        height :40px;
        width : 100%;
        color : white;
        background-color: #191919;
        margin-top: 12%;
        text-align: center;

        }
.styleP{
        font-size:50px;
        color : red;
        }
        .maincontainer{
            height:100%;
            width: 90%;
            margin-left: 10%;
           
           
            
        }
        .maincontainertable{
         
            width: 85%;
            background-color: #79b7e7;
            
            
        }
       
        .medications{
            
            border: 2px;
            alignment-adjust: auto;
            margin-left: 10%;
            overflow: scroll; 
            width : 600px;
            height: 180px;
                
        }
        body {
  
    background-color: #cccccc;
}
.endtable{
    width: 750px;
     height: 250px ;
    margin-left: 5%;
}

        </style>
        
<!--        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery.min.js"></script>-->
<link href="jquery.datepick.package-5.0.0/jquery.datepick.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.plugin.js"></script>
<script src="jquery.datepick.package-5.0.0/jquery.datepick.js"></script>
<script>
$(function() {
	$('#popupDatepicker').datepick();
        $('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	alert('The date chosen is ' + date);
}
</script>
  
<script>
    $( "#other" ).click(function() {
  $( "#target" ).scroll();
});
</script>
    </head>
    <body  >
        
        <%@include file="menu.html"%>
        
        <div class="maincontainer" >
        <h3><b>Update Patient</b></h3>
        <br/>
       
        <%
        	//String username = (String) session.getAttribute("username");
        	//out.println(username);
        %>
        
        <form method="post" action="updateServlet" enctype="multipart/form-data">

		<%
			String pid =request.getParameter("id");
			String pname=request.getParameter("name");
        	String paddress=request.getParameter("address");
     		String pdob=request.getParameter("dob");
            String pemail=request.getParameter("email");
        	String pcontact=request.getParameter("contact");
        	String pgender=request.getParameter("gender");
        	String pheight=request.getParameter("height");
        	String pweight=request.getParameter("weight");
        	String pbgroup=request.getParameter("bgroup");
        	String pmstatus=request.getParameter("mstatus");
        	String poccpation=request.getParameter("occupation");
        	
        /*	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");

        	  Date date=(Date) sdf.parse(pdob);

        	sdf=new SimpleDateFormat("MM/dd/yyyy");
        	System.out.println(sdf.format(date));
		*/
		
		String ds1 = "2007-06-30";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
		String ds2 = sdf2.format(sdf1.parse(pdob));
		//System.out.println(ds2); //will be 30/06/2007
		%>
		  <input type="hidden" value="<%=pid %>" name="id">
 <table class="endtable">
 
       
         <tr>
              <th  style= "text-align: right;">Name of Patient&nbsp;&nbsp;&nbsp; </th>
             <td ><input type="text" style="width: 150px"   name="ptname" value=<%=pname %>></td>
           <th style= "text-align: right;">Contact No.&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="contact" value=<%=pcontact %>></td>
        </tr>
         <tr>
             <th style= "text-align: right;">Gender&nbsp;&nbsp;&nbsp; </th>
            <td >	<select name="gender">
					<option value=<%=pgender %>><%=pgender %></option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
					</select>
			</td>
           <th style= "text-align: right;">Marital Status&nbsp;&nbsp;&nbsp;</th>
           <td ><select name="mstatus">
           			<option value=<%=pmstatus %>><%=pmstatus %></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select></td>
        </tr>
         <tr>
             <th style= "text-align: right;">Height&nbsp;&nbsp;&nbsp; </th>
            <td ><input type="text" style="width: 150px"   name="height" value=<%=pheight%>></td>
           <th style= "text-align: right;">Occupation&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="occupation" value=<%=poccpation %>></td>
        </tr>
        <tr>
             <th style= "text-align: right;">Weight&nbsp;&nbsp;&nbsp; </th>
            <td ><input type="text" style="width: 150px"   name="weight" value=<%=pweight %>></td>
           <th style= "text-align: right;">Email&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px"   name="email" value=<%=pemail %>></td>
        </tr>
        
        <tr>
             <th style= "text-align: right;">Blood Group &nbsp;&nbsp;&nbsp; </th>
            <td><select name="bgroup">
            		<option value=<%=pbgroup %>><%=pbgroup %></option>
					<option value="O+">O+</option>
					<option value="O-">O-</option>
					<option value="A+">A+</option>
					<option value="A-">A-</option>
					<option value="B+">B+</option>
					<option value="B-">B-</option>
					<option value="AB+">AB+</option>
					<option value="AB-">AB-</option>
					</select></td>
            <th style= "text-align: right;">Date Of Birth&nbsp;&nbsp;&nbsp;</th>
           <td ><input type="text" style="width: 150px" id="popupDatepicker"  name="dob" value=<%=ds2 %>></td>
           
        </tr>
         <tr>
              <th style= "text-align: right;">Patient Picture &nbsp;&nbsp;&nbsp; </th>
              <td ><input type="file" style="width: 250px"   name="photo" value="Browse"></td>
           
        </tr>
        <tr>
              <th style= "text-align: right;"></th>
             
              <td ><img src="<%=request.getContextPath()%>/images/test.jpg" style="width: 100px; height: 100px"   name="image"></td>
           <th style= "text-align: right;">Address&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/></th>
           <td ><textarea rows="1" cols="30" name="ptaddress" >
           		<%=paddress.trim()%>
           		</textarea>

           <br/>
              <br/><input type='image' height='50' width="130" src='images/submit.png'><br/></td>
        </tr>
         
     
       </table>
     </form>
     
            <%@include file="showPatientTable.jsp" %>
       
    </div>
            
            
           
<div>


</div>
</form>


 
            <div class="footer">


<div  style="padding:10px 5px 15px 20px;">
    © 2014 Scripting Logic - All Rights Reserved.
</div> 
    </body>
</html>
