package com.ImageActions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static com.dbcon.MyDriver.*;
@WebServlet("/imageServletUser")

public class RetrieveImageDb extends HttpServlet
{
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
		service(request, response);
	
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
		service(request, response);
		
	}
	protected void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
	{
		 Connection con=null;
		 String sid=request.getParameter("id");
		 PrintWriter out = response.getWriter();
		 Boolean flag = true;
		 try
			{
				 Class.forName(DB_DRIVER);
		         con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
		        //ArrayList<Double> arr = new ArrayList<Double>();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select profile_photo from clinic_user where id ="+sid+";");
				//ResultSet rs1 = stmt.executeQuery("select patient_id from clinic_patient;");
				
				int i=0;
			
				while (rs.next()) {
					//out.println("in while loop....");
					InputStream in = rs.getBinaryStream(1);
					OutputStream f = new FileOutputStream(new File("C:\\Users\\SWAPNALI\\git\\dplus\\WebContent\\images\\test.jpg"));
					i++;
					int c = 0;
					 byte[] buffer = new byte[1];
					while ((in.read(buffer)) > 0) {
					//	out.println("in while write loop....");
						f.write(buffer);
					}
					f.close();
					in.close();
				//	out.println("out while write loop....");
					
					getServletContext().getRequestDispatcher("/updateUser.jsp").forward(request, response);
					//getServletContext().getRequestDispatcher("/myimageServlet").include(request, response);
				}
			}catch(Exception ex)
			{
				out.println(ex.getMessage());
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		 
	}
}
