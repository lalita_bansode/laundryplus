package com.ImageActions;


import static com.dbcon.MyDriver.*;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/uploadServletUser")
@MultipartConfig(maxFileSize = 16177215)
public class AddUserServlet extends HttpServlet 
{
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
	{
		
		 String sname=request.getParameter("name");
		 // System.out.println("name = " + sname);
         String sgender=request.getParameter("gender");
         String sdob=request.getParameter("dob");
         String saddress=request.getParameter("address");
         String scontact=request.getParameter("contact");
         String semail=request.getParameter("email");
         String sdesg=request.getParameter("designation");
         //String susername=request.getParameter("uname");
         String spassword=request.getParameter("pass");
         
         InputStream inputstream = null;
         Part filePart=request.getPart("pphoto");
         if(filePart != null)
         {
        	 inputstream=filePart.getInputStream();
         }
         Connection conn = null;	// connection to the database
 		 String message = null;	// message will be sent back to client
 		
 		try
 		{
 			String S1="insert into clinic_user(name,gender,date_of_birth,address,contact_no,email_id,designation,profile_photo,password,clinic_id) values(?,?,?,?,?,?,?,?,?,?)";
 			String S2="select * from tempclinic";
 			
 			 System.out.println(S1);
 			Class.forName(DB_DRIVER);
            Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
            Statement st=con.createStatement();
        
             
             ResultSet rs =st.executeQuery(S2);
             BigDecimal clinic_id = null ;
             while(rs.next())
             {
               
             
                clinic_id =  rs.getBigDecimal("clinic_id");
             }
             
             

               DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
               java.util.Date pdate = df.parse(sdob);
               System.out.println("date inserted= " + pdate);
             java.sql.Date d1= new java.sql.Date(pdate.getTime());
             System.out.println("date inserted= " + d1);
             
            	PreparedStatement statement = con.prepareStatement(S1);
 			
            statement.setString(1, sname);
 			statement.setString(2, sgender);
 			statement.setDate(3, d1);
 			statement.setString(4, saddress);
 			statement.setString(5, scontact);
 			
 			statement.setString(6, semail);
 			statement.setString(7, sdesg);
 			statement.setBlob(8, inputstream);
 			statement.setString(9,spassword);
 			statement.setBigDecimal(10, clinic_id);
 			
 			
 			// sends the statement to the database server
 			int row = statement.executeUpdate();
 			if (row > 0) {
 				message = "File uploaded and saved into database";
 				System.out.println(message);
 				
 			}
            
 		}
 		catch(SQLException | ClassNotFoundException | ParseException ex)
 		{
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} 
		finally 
		{
			if (conn != null)
				{
				// closes the database connection
				try
				{
					conn.close();
				} 
				catch (SQLException ex)
				{
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			request.setAttribute("Message", message);
			
			// forwards to the message page
			getServletContext().getRequestDispatcher("/addUser.jsp").forward(request, response);
			
		}
	}
	

}
