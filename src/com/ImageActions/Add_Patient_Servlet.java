package com.ImageActions;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import static com.dbcon.MyDriver.*;

@WebServlet("/uploadServlet")
@MultipartConfig(maxFileSize = 16177215)	// upload file's size up to 16MB
public class Add_Patient_Servlet extends HttpServlet{
	
	
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// gets values of text fields
		  String pname=request.getParameter("ptname");
          String paddress=request.getParameter("ptaddress");
        // System.out.println("address = " + paddress);
          String pdob=request.getParameter("dob");
         // System.out.println("date = " + pdob);
          String pemail=request.getParameter("email");
          String pcontact=request.getParameter("contact");
          String pgender=request.getParameter("gender");
          String pheight=request.getParameter("height");
          String pweight=request.getParameter("weight");
          String pbgroup=request.getParameter("bgroup");
          String pmstatus=request.getParameter("mstatus");
          String poccpation=request.getParameter("occupation");
          String pdoctor=request.getParameter("dname");
		
          
         /* StringBuffer paddress = new StringBuffer(request.getParameter("ptadd"));
          
          int loc = (new String(paddress)).indexOf('\n');
          while(loc > 0){
              paddress.replace(loc, loc+1, "<BR>");
              loc = (new String(paddress)).indexOf('\n');
         }
         System.out.println("address = " + paddress); */
		InputStream inputStream = null;	// input stream of the upload file
		
		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("photo");
		if (filePart != null) {
			// prints out some information for debugging
			//System.out.println(filePart.getName());
			//System.out.println(filePart.getSize());
			//System.out.println(filePart.getContentType());
			
			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}
		
		Connection conn = null;	// connection to the database
		String message = null;	// message will be sent back to client
		
		try {
			// connects to the database
			
			 String Sql="insert into clinic_patient(patient_name,patient_address,patient_contact,patient_email,patient_dob,patient_gender,patient_marital_status,patient_height,patient_weight,patient_bloodgroup,patient_profile_photo,patient_occupation,clinic_id,patient_doctor_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			// String Sql="insert into clinic_patient(patient_name,patient_address,patient_contact,patient_email,patient_dob,patient_gender,patient_marital_status,patient_height,patient_weight,patient_bloodgroup,patient_profile_photo,patient_occupation,clinic_id) values (" +pname+ ",'"+paddress+"' ,'"+pcontact+"',"+pemail+",'"+pdob+"',"+pgender+" ,'"+pmstatus+"' ,'"+pheight+"',"+pweight+",'"+pbgroup+"',"+poccpation+")";;
			 String sql1 = "select * from tempclinic";
	          
	          
	          
         System.out.println(Sql);
            // out.println("Sql query"+Sql +"<br/>");
            Class.forName(DB_DRIVER);
            Connection con=DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
            
            Statement st=con.createStatement();

            
            ResultSet rs =st.executeQuery(sql1);
            BigDecimal clinic_id = null ;
            while(rs.next())
            {
              
            
               clinic_id =  rs.getBigDecimal("clinic_id");
            }
            
            

              DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
              java.util.Date pdate = df.parse(pdob);
              System.out.println("date inserted= " + pdate);
            java.sql.Date d1= new java.sql.Date(pdate.getTime());
            System.out.println("date inserted= " + d1);
            
           	PreparedStatement statement = con.prepareStatement(Sql);
			
           	statement.setString(1, pname);
			statement.setString(2, paddress);
			statement.setString(3, pcontact);
			statement.setString(4, pemail);
			statement.setDate(5, d1);
			statement.setString(6, pgender);
			statement.setString(7, pmstatus);
			statement.setString(8, pheight);
			statement.setString(9, pweight);
			statement.setString(10, pbgroup);
			statement.setBlob(11, inputStream);
			statement.setString(12, poccpation);
			statement.setBigDecimal(13, clinic_id);
			statement.setString(14, pdoctor);
			
			// sends the statement to the database server
			int row = statement.executeUpdate();
			if (row > 0) {
				message = "File uploaded and saved into database";
				System.out.println(message);
				
			}
		} catch (SQLException | ClassNotFoundException | ParseException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			request.setAttribute("Message", message);
			
			// forwards to the message page
			getServletContext().getRequestDispatcher("/registerPatient.jsp").forward(request, response);
			
		}
	}
}