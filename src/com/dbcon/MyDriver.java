package com.dbcon;

import java.util.ResourceBundle;

public class MyDriver
{
	static ResourceBundle dbDetails = ResourceBundle.getBundle("dbSource");
	
	public static final String DB_DRIVER=dbDetails.getString("DbDriver");
	public static final String DB_URL=dbDetails.getString("DbUrl");
	public static final String DB_USER=dbDetails.getString("UserName");
	 public static final String DB_PASS=dbDetails.getString("Password");
	
	public static void main(String args[]) throws ExceptionInInitializerError
	{
		System.out.println(DB_URL);
		System.out.println(DB_USER);
		System.out.println(DB_PASS);
	}
	
}
